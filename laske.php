<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nettopalkka</title>
</head>
<body>
  <?php
  $bruttopalkka = filter_input(INPUT_POST,'brutto',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
  $ennakkopidatys  = filter_input(INPUT_POST,'ennakko',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
  $tyoelakemaksu  = filter_input(INPUT_POST,'elake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
  $tyottomyysvakuutusmaksu = filter_input(INPUT_POST,'vakuutus',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);

  $ennakko = $bruttopalkka / 100 * $ennakkopidatys;
  $elake = $bruttopalkka / 100 *  $tyoelakemaksu;
  $vakuutus = $bruttopalkka / 100 *  $tyottomyysvakuutusmaksu;

  $vahennykset = $ennakko+$elake+$vakuutus;

  $nettopalkka = ($bruttopalkka - $vahennykset);
  
  printf("<p>Nettopalkka on $nettopalkka € </p>");
  printf("<p>Ennakkopidätys on $ennakko € </p>");
  printf("<p>Työeläkemaksu on $elake € </p>");
  printf("<p>Työttömyysvakuutusmaksu on $vakuutus € </p>");
  ?>
  <a href="index.html">Laske uudestaan</a>
</body>
</html>